package her.blok2.b_ict.hz.com.blok2her.model;

/**
 * Created by anton on 8-1-16.
 * Song is part of an Album
 */
public class Song extends MusicItem{
    //fields for Song
    private int length;
    private String title;
    private Album album;

    public Song() {
    }

    public Song(String title, Album album) {
        this.title=title;
        this.album=album;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }
}
